#include "contract.h"

#include <string>

namespace ggstl
{
namespace ctrct
{
namespace violation
{
   struct contract
   {
      /// Default constructor
      /// (Useful for contracts with no information)
      constexpr contract() :
         supplier{""},
         clause{""}
      {}

      /// Parametered constructor
      /// \param[in] supplier Whose contract this is
      /// \param[in] clause What clause this is
      constexpr contract(
         const char* supplier,
         const char* clause)
         :
         supplier{ supplier },
         clause{ clause }
      {}

      /// Function that supplies the contract.
      /// Non-owning!
      const char* supplier;

      /// Description of the contract clause.
      /// Non-owning!
      const char* clause;

      /// Contract handle
      /// \note Pruposely only declared, not defined.
      ///       Needs to be defined by the application.
      void handle() const;

   };
}

   /// Declares a contract clause (enabled specialization)
   /// \see ggstl::ctrct::contract<bool>(bool,violation::contract const&).
   template<>
   inline void contract<true>(
      bool condition,
      violation::contract const& c)
   {
      if (!condition) { c.handle(); }
   }

   /// Declares a contract clause (disabled specialization)
   /// \see ggstl::ctrct::contract<bool>(bool,violation::contract const&).
   template<>
   inline void contract<false>(
      bool condition,
      violation::contract const&)
   {}
}
}