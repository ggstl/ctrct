#pragma once

#include "contract.h"

/// Macro used to define the default postcondition state.
/// This must be convertible to boolean.
/// \see GGSTL_CTRCT_CONTRACT_ENABLED
#ifndef GGSTL_CTRCT_POSTCONDITION_ENABLED
#define GGSTL_CTRCT_POSTCONDITION_ENABLED GGSTL_CTRCT_CONTRACT_ENABLED
#endif

namespace ggstl
{
   namespace ctrct
   {
      /// Default postcondition enabled value.
      constexpr bool postcondition_enabled = GGSTL_CTRCT_POSTCONDITION_ENABLED;

      namespace violation
      {
         /// Contains information concerning the postcondition
         struct postcondition;
      }

      /// Declares a postcondition clause
      /// If enabled is true, will check the condition 
      /// and call the handler when condition is false.
      /// If enabled is false, does nothing.
      /// \param condition    condition to check.
      /// \param clause       clause containing postcondition information.
      template<bool enabled = contract_enabled>
      inline void postcondition(
         bool condition,
         violation::postcondition const& clause = {}
      );
   }
}
/// Include implementation templates.
#include "postcondition.hh"
