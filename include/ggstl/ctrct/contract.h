#pragma once

/// Macro used to define the default contract state.
/// This must be convertible to boolean.
/// -true   contracts are enabled.
/// -false  contracts are disabled.
#ifndef GGSTL_CTRCT_CONTRACT_ENABLED
#define GGSTL_CTRCT_CONTRACT_ENABLED true
#endif

namespace ggstl
{
namespace ctrct
{
   /// Default contract enabled value.
   constexpr bool contract_enabled = GGSTL_CTRCT_CONTRACT_ENABLED;

   namespace violation
   {
      /// Contains information concerning the contract
      struct contract;
   }

   /// Declares a contract clause
   /// If enabled is true, will check the condition 
   /// and call the handler when condition is false.
   /// If enabled is false, does nothing.
   /// \param condition    condition to check.
   /// \param clause       clause containing contract information.
   template<bool enabled = contract_enabled>
   inline void contract(
      bool condition, 
      violation::contract const& clause = {}
   );

}
}
/// Include implementation templates.
#include "contract.hh"