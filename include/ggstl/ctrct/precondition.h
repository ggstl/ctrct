#pragma once

#include "contract.h"

/// Macro used to define the default precondition state.
/// This must be convertible to boolean.
/// \see GGSTL_CTRCT_CONTRACT_ENABLED
#ifndef GGSTL_CTRCT_PRECONDITION_ENABLED
#define GGSTL_CTRCT_PRECONDITION_ENABLED GGSTL_CTRCT_CONTRACT_ENABLED
#endif

namespace ggstl
{
   namespace ctrct
   {
      /// Default precondition enabled value.
      constexpr bool precondition_enabled = GGSTL_CTRCT_PRECONDITION_ENABLED;

      namespace violation
      {      
         /// Contains information concerning the precondition
         struct precondition;
      }

      /// Declares a precondition clause
      /// If enabled is true, will check the condition 
      /// and call the handler when condition is false.
      /// If enabled is false, does nothing.
      /// \param condition    condition to check.
      /// \param clause       clause containing precondition information.
      template<bool enabled = precondition_enabled>
      inline void precondition(
         bool condition,
         violation::precondition const& = {}
      );
   }
}
/// Include implementation templates.
#include "precondition.hh"