#include "precondition.h"

namespace ggstl
{
   namespace ctrct
   {
      namespace violation
      {
         struct precondition
            :
            contract
         {
            using contract::contract;

            /// Precondition handle
            /// \note Pruposely only declared, not defined.
            ///       Needs to be defined by the application.
            void handle() const;
         };
      }

      template<>
      inline void precondition<true>(
         bool condition,
         violation::precondition const& c)
      {
         if (!condition) { c.handle(); }
      }

      template<>
      inline void precondition<false>(
         bool condition,
         violation::precondition const&)
      {}
   }
}