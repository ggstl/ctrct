#include "postcondition.h"

namespace ggstl
{
   namespace ctrct
   {
      namespace violation
      {
         struct postcondition
            :
            contract
         {
            using contract::contract;

            /// Postcondition handle
            /// \note Pruposely only declared, not defined.
            ///       Needs to be defined by the application.
            void handle() const;
         };
      }

      /// Declares a precondition clause (enabled specialization)
      /// \see ggstl::ctrct::precondition<bool>(bool,violation::precondition const&).
      template<>
      inline void postcondition<true>(
         bool condition,
         violation::postcondition const& c)
      {
         if (!condition) { c.handle(); }
      }

      /// Declares a precondition clause (disabled specialization)
      /// \see ggstl::ctrct::precondition<bool>(bool,violation::precondition const&).
      template<>
      inline void postcondition<false>(
         bool condition,
         violation::postcondition const&)
      {}
   }
}