#include <ggstl/ctrct.h>

int sqrt(int i)
{
   ggstl::ctrct::precondition(i >= 0,
   { __func__,"input must be positive"});
   
   auto ret = 0;
   for (; ret*ret <= i; ++ret);
   --ret;

   ggstl::ctrct::postcondition(ret >= 0,
   { __func__,"return value must be positive"});
   ggstl::ctrct::postcondition((ret)*(ret) <= i,
   { __func__,"return value squared must be smaller or equal than input" });

   return ret;
}

int main()
{
   sqrt(-1);
   sqrt(0);
   sqrt(1);
   sqrt(2);
}
