/// File: handler.cc
///
/// This file declares all the ggstl::ctrct handlers.

#include <ggstl/ctrct.h>

#include <iostream>
#include <assert.h>

using namespace ggstl::ctrct::violation;

/// Called when a contract() evaluates to false.
void contract::handle() const
{
   std::cout << "[CTRCT]:" << supplier << " " << clause << std::endl;
}

/// Called when a precondition() evaluates to false
void precondition::handle() const
{
   contract::handle();
}

/// Called when a postcondition() evaluates to false
void postcondition::handle() const
{
   contract::handle();
}