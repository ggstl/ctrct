# CTRCT library

The *ctrct* library is a C++ header-only design-by-contract library, focused on a 
minimalistic approach. It allows developpers to declare contract clauses 
directly inside their code, and define contract filtering and handling at 
compile-time. This library is based on the excellent article 
[N4075](http://www.open-std.org/JTC1/SC22/WG21/docs/papers/2014/n4075.pdf) by 
Lakos, Zakharov and Beels.

## What are contracts?

Contracts can be many things. 
- They are part of the interface of a software unit;
- They are agreements between software units;
- They are a verification tool;
- but MOST OF ALL, they are a development tool;

Contracts are really just detacheable defensive code inside software units. 
They check conditions, and handle false conditions. You can decide whether or 
not to keep them and how to handle violations at compile-time.

## What are contracts not?

Contracts are not a reliable run-time error-handling solution. They are not 
meant to be relied on during execution.

TL;DR: Contracts are 'smart' asserts

### Why do I need contracts?

Contracts help detect faults earlier, are a great addition to other test 
mechanisms, and offer free documentation on code behaviour.


## How to install

As this is a header-only library, you can simply download all the files in your
favorite environment and make your compiler include them.

## Examples?

Sure!

### Contracted function
```cpp
// Compute the integer square root
// returns the largest integer s that satisfies s*s <= i
int sqrt(int i)
{
    // preconditions
    ggstl::ctrct::precondition(i >= 0);
    
    int s = i;
    while(s*s > i) --s;
    
    // postconditions
    ggstl::ctrct::postcondition(s >= 0);
    ggstl::ctrct::postcondition(s*s <= i);
    ggstl::ctrct::postcondition((s+1)*(s+1) > i);
    
    return s;
}
```

### Adding information

```cpp
void foo(int i)
{
    ggstl::ctrct::precondition(i % 2 == 0,
    {__func__, "i must be pair for some reason"});
}
```

### Handling violations
```cpp
// Declare the handler once anywhere in your code.
// It will be called whenever a contract() clause
// evaluates to false.
void ggstl::ctrct::violation::contract::handle() const
{
    std::cout << "[ctrct]" << supplier << ": " << clause << std::endl;
}

// Same for the other contract types!
void ggstl::ctrct::violation::precondition::handle() const
{
    ggstl::ctrct::violation::contract::handle();
}
```

Possible Output:

`[ctrct]foo: i must be pair for some reason`

### Enabling/Disabling contracts

```cpp
constexpr bool are_contracts_enabled = true;

void bar(int a, int b, int c)
{
    // Default contract
    // Bound by default contract status
    ggstl::ctrct::precondition(a > 0);

    // Always enabled contract
    ggstl::ctrct::precondition<true>(b > 0);
    
    // Always disabled contract
    ggstl::ctrct::precondition<false>(c > 0);
    
    // Variable-dependant contract
    ggstl::ctrct::precondition<are_contracts_enabled>(a > b);
}
```

## Interested?

Check out the [Wiki](https://gitlab.com/ggstl/ctrct/wikis/home)!

## Still unsure?

Why not dive into [the code](/include/ggstl/) and check it out yourself. 
(it's not that scary)
